<?php

if (empty($argv[1]) || empty($argv[2])) {
    die("usage: crawler.php id url\n");
}

$id = intval($argv[1]);
$url = rawurldecode($argv[2]);

if (filter_var($url, FILTER_VALIDATE_URL) === false) {
    die("Wrong URL\n");
}

require_once 'functions.php';

$update_data = [
    ':id' => $id,
    ':og_data' => null,
];

list($error, $page) = fetch($url);

if ($error) {
    $update_data[':status'] = OGP_STATUS_ERROR;
} else {
    $update_data[':status'] = OGP_STATUS_DONE;
    $update_data[':og_data'] = parseOG($page);
}

$dbh = dbConnect();

try {
    $sql = 'UPDATE stories SET 
      `status` = :status, 
      `fetched_at` = NOW(), 
      `og_data` = :og_data 
      WHERE `id` = :id';
    $sth = $dbh->prepare($sql);
    $sth->execute($update_data);
} catch (PDOException $e) {
    die($e->getMessage());
} catch (Exception $e) {
    die('Unknown error');
}
