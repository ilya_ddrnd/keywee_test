<?php

define('OGP_STATUS_PENDING', 0);
define('OGP_STATUS_DONE', 1);
define('OGP_STATUS_ERROR', 2);

function replyWithError($error) {
    die(json_encode(['error' => $error]));
}

function dbConnect() {
    $dsn = 'mysql:dbname='. $_ENV['MYSQL_DATABASE'] .';host=' . $_ENV['MYSQL_HOST'];
    $user = $_ENV['MYSQL_USER'];
    $password = $_ENV['MYSQL_PASSWORD'];

    try {
        $dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        replyWithError ('Connection failed: ' . $e->getMessage());
    }

    return $dbh;
}

function getStatus($status) {
    switch ($status) {
        case OGP_STATUS_PENDING:
            return 'pending';
        case OGP_STATUS_DONE:
            return 'done';
        case OGP_STATUS_ERROR:
        default:
            return 'error';
    }
}

function callCrawler($id, $url) {
    exec('php ../scripts/crawler.php ' . $id . ' ' . rawurlencode($url) . ' > /dev/null 2>&1 &');
}

function fetch($url) {
    $curl = curl_init($url);

    $useragent = !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36';

    curl_setopt($curl, CURLOPT_FAILONERROR, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 15);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_USERAGENT, $useragent);

    $response = curl_exec($curl);

    curl_close($curl);

    return [$response === false, $response];
}

function parseOG($html) {
    $special_arrays = ['image', 'video', 'audio'];

    $special_flat_arrays = [
        'locale:alternate',
        'music:album',
        'music:musician',
        'video:actor',
        'video:director',
        'video:writer',
        'video:tag',
        'article:author',
        'article:tag',
        'book:author',
        'book:tag'
    ];

    $old_libxml_error = libxml_use_internal_errors(true);

    $doc = new DOMDocument();
    $doc->loadHTML($html);

    libxml_use_internal_errors($old_libxml_error);

    $tags = $doc->getElementsByTagName('meta');
    if (!$tags || $tags->length === 0) {
        return null;
    }

    $data = [];

    $special_key_index = [];

    foreach ($tags AS $tag) {
        if ($tag->hasAttribute('property') &&
            substr($tag->getAttribute('property'), 0, 3) == 'og:' &&
            $tag->hasAttribute('content')
        ) {
            $key = substr($tag->getAttribute('property'), 3);
            $value = $tag->getAttribute('content');

            if (in_array($key, $special_flat_arrays)) {
                $data[$key][] = $value;
            } else {
                $base_key = substr($key, 0, 5);
                if (in_array($base_key, $special_arrays)) { // if image, video or audio
                    if ($key == $base_key) {
                        //increase array index
                        if (array_key_exists($base_key, $special_key_index)) {
                            $special_key_index[$base_key]++;
                        } else {
                            $special_key_index[$base_key] = 0;
                        }
                        $data[$base_key][$special_key_index[$base_key]]['url'] = $value;
                    } else {
                        if (strlen($key) > 6) {
                            $subkey = substr($key, 6);
                            $data[$base_key][$special_key_index[$base_key]][$subkey] = $value;
                        }
                    }
                } else {
                    $data[$key] = $value;
                }
            }
        }
    }

    if (empty($data)) {
        return null;
    }

    return json_encode($data);
}