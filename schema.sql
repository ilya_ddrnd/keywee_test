CREATE TABLE `stories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `url` varchar(16384) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `added_at` datetime NOT NULL,
  `fetched_at` datetime NULL,
  `og_data` json NULL
) ENGINE='InnoDB' COLLATE 'utf8_bin';

ALTER TABLE `stories`
ADD UNIQUE `url` (`url`(64));


CREATE TABLE `status` (
  `id` tinyint(3) unsigned NOT NULL,
  `status` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `status` (`id`, `status`) VALUES
(0,	'pending'),
(1,	'done'),
(2,	'error');