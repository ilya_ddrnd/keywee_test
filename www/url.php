<?php

require_once '../scripts/functions.php';

if (strtoupper($_SERVER['REQUEST_METHOD']) !== 'POST') {
    http_response_code(405);
    exit;
}

header("Content-type: application/json; charset=utf-8");

if (empty($_GET['url']) || filter_var($_GET['url'], FILTER_VALIDATE_URL) === false) {
    replyWithError('Not a valid URL');
}

$url = $_GET['url'];

$dbh = dbConnect();

try {
    $sql = 'SELECT id FROM stories WHERE url = :url';
    $sth = $dbh->prepare($sql);
    $data = [':url' => $url];
    $sth->execute($data);
    $result = $sth->fetch(PDO::FETCH_ASSOC);
    $sth->closeCursor();

    if (!empty($result['id'])) {
        callCrawler($result['id'], $url);
        die(json_encode(['id' => $result['id']]));
    }

    $sql = 'INSERT INTO stories (`url`, `added_at`) VALUES(:url, NOW())';
    $sth = $dbh->prepare($sql);
    $data = [':url' => $url];
    $sth->execute($data);
    $lastId = $dbh->lastInsertId();
    $sth->closeCursor();

} catch (PDOException $e) {
    replyWithError($e->getMessage());
} catch (Exception $e) {
    replyWithError('Unknown error');
}

callCrawler($lastId, $url);

die(json_encode(['id' => $lastId]));


