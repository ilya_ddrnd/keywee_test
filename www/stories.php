<?php

require_once '../scripts/functions.php';

if (strtoupper($_SERVER['REQUEST_METHOD']) !== 'GET') {
    http_response_code(405);
    exit;
}

header("Content-type: application/json; charset=utf-8");

if (empty($_GET['id'])) {
    replyWithError('Empty id');
}

$id = $_GET['id'];

$dbh = dbConnect();

try {
    $sql = 'SELECT `id`, `url`, `status`, `fetched_at`, `og_data` FROM stories WHERE id = :id';
    $sth = $dbh->prepare($sql);
    $sql_data = [':id' => $id];
    $sth->execute($sql_data);
    $result = $sth->fetch(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    replyWithError($e->getMessage());
} catch (Exception $e) {
    replyWithError('Unknown error');
}

if (empty($result)) {
    replyWithError('Id not found');
}

$response = [
    'url' => $result['url'], // given URL
    'id' => $result['id'],
    'scrape_status' => getStatus($result['status']),
];

if ($result['status'] != OGP_STATUS_PENDING) {
    $response['updated_time'] = $result['fetched_at'];

    if (!empty($result['og_data'])) {
        $data = json_decode($result['og_data']);

        foreach ($data as $key => $value) {
            $response[$key] = $value;
        }
    }
}

die(json_encode($response));


