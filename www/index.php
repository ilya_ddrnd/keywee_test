<?php

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Keywee home assignment</title>
</head>
<body>
<h2>Examples</h2>
<h3>Post URL</h3>
<code>
    curl --request POST --url http://IlyaKolesnikov.hiring.keywee.io/stories?url=http%3A%2F%2Fogp.me%2F
</code>

<h3>Get data</h3>
<code>
    curl --request GET --url http://IlyaKolesnikov.hiring.keywee.io/stories/1
</code>

<h3>Postman examples</h3>
<p>Download and import file: <a href="/keywee_test_examples.postman_collection.json">keywee_test_examples.postman_collection.json</a>
</p>
<p><a target="_blank" href="https://www.getpostman.com/">Read more</a> about Postman</p>

</body>
</html>
